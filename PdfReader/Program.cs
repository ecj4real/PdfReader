﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.IO;

namespace PdfFileReader
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "C:/Users/ECJ4REAL.ECJ4REAL-PC/Desktop/mypdffile.pdf";
            string text = string.Empty;
            PdfReader reader = new PdfReader(path);
            for(int page = 1; page <= reader.NumberOfPages; page++)
            {
                text += PdfTextExtractor.GetTextFromPage(reader, page);
            }
            reader.Close();

            Console.WriteLine(text);
            Console.ReadKey();
        }
    }
}
